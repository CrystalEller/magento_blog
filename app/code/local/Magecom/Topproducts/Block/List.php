<?php

class Magecom_Topproducts_Block_List extends Mage_Core_Block_Template
{
    public function getLastAddedProducts()
    {
        $collection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToSort('entity_id', 'desc');

        $collection->getSelect()->limit(3);


        return $collection;
    }
}