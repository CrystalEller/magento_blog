<?php

class Magecom_Blog_Model_Resource_CategoryPost extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('magecom_blog/category_post', 'category_post_id');
    }
}