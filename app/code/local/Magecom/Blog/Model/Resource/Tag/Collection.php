<?php

class Magecom_Blog_Model_Resource_Tag_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('magecom_blog/tag');
        parent::_construct();
    }

    public function addPostFilter($postId)
    {
        $this->getSelect()->join(array('tag_post' => 'magecom_blog_tag_post'), 'id = tag_post.tag_id', array())
            ->where('tag_post.post_id = ?', $postId);

        return $this;
    }
}