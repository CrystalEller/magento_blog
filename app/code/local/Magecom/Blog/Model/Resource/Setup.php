<?php

class Magecom_Blog_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup
{

    public function getDefaultEntities()
    {
        $entities = array(
            'magecom_blog_post' => array(
                'entity_model' => 'magecom_blog/post',
                'attribute_model' => 'catalog/resource_eav_attribute',
                'table' => 'magecom_blog/post_entity',
                'attributes' => array(
                    'title' => array(
                        'type' => 'varchar',
                        'label' => 'Title',
                        'input' => 'text',
                        'global' => 0,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => true,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                    ),
                    'description' => array(
                        'type' => 'text',
                        'label' => 'Title',
                        'input' => 'text',
                        'global' => 0,
                        'visible' => true,
                        'required' => true,
                        'user_defined' => true,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                    ),
                    'image_url' => array(
                        'type' => 'varchar',
                        'label' => 'Image',
                        'input' => 'text',
                        'global' => 0,
                        'visible' => true,
                        'user_defined' => true,
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => true,
                        'unique' => false,
                    ),
                ),
            ),
        );
        return $entities;
    }
}