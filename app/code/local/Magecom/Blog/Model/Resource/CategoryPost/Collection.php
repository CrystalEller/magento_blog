<?php

class Magecom_Blog_Model_Resource_CategoryPost_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('magecom_blog/categoryPost');
        parent::_construct();
    }
}