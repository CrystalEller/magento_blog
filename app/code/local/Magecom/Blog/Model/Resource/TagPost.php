<?php

class Magecom_Blog_Model_Resource_TagPost extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('magecom_blog/tag_post', 'tag_post_id');
    }
}