<?php

class Magecom_Blog_Model_Resource_Category_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('magecom_blog/category');
        parent::_construct();
    }

    public function addPostFilter($postId)
    {
        $this->getSelect()->join(array('category_post' => 'magecom_blog_category_post'), 'id = category_post.category_id', array())
            ->where('category_post.post_id = ?', $postId);

        return $this;
    }
}