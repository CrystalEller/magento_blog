<?php

class Magecom_Blog_Model_Validation_PostsQty extends Mage_Core_Model_Config_Data
{
    public function save()
    {
        $posts_qty = $this->getValue(); //get the value from our config

        $validator = new Zend_Validate_Int();

        if(!$validator->isValid($posts_qty)) {
            Mage::throwException('Field "Qty of posts on main page" must be int');
        } else {
            return parent::save();
        }
    }
}