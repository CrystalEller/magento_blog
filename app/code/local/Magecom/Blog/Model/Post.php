<?php

class Magecom_Blog_Model_Post extends Mage_Core_Model_Abstract
{
    private $isSaved = false;

    public function _construct()
    {
        $this->_init('magecom_blog/post');
        parent::_construct();

        if (Mage::app()->getStore()->isAdmin()) {
            $this->setStoreId(Mage::app()->getRequest()->getParam('store', 0));
        }
        else{
            $this->setStoreId(Mage::app()->getStore()->getId());
        }
    }

    public function validate()
    {
        $errors = array();
        $cats = $this->getCats();
        $tags = $this->getTags();

        if (!Zend_Validate::is($this->getTitle(), 'NotEmpty')) {
            $errors[] = 'Title is needed';
        }

        if (!Zend_Validate::is($this->getDescription(), 'NotEmpty')) {
            $errors[] = 'Description is needed';
        }

        /*if (!Zend_Validate::is($cats, 'NotEmpty')) {
            $errors[] = 'Category is needed';
        } elseif (is_array($cats)) {
            $catsIds = array_map('intval', $cats);

            foreach ($catsIds as $cId){
                $size = Mage::getModel('magecom_blog/categoryPost')
                    ->getCollection()
                    ->addFieldToFilter('category_id', array('eq' => array($cId)))
                    ->getSize();

                if($size == 0) {
                    $errors[] = 'Category error';
                    break;
                }
            }
        }*/

        if (!Zend_Validate::is($tags, 'NotEmpty')) {
            if (is_array($tags)) {
                $tagsIds = array_map('intval', $tags);

                foreach ($tagsIds as $tId){
                    $size = Mage::getModel('magecom_blog/tagPost')
                        ->getCollection()
                        ->addFieldToFilter('tag_id', array('eq' => array($tId)))
                        ->getSize();

                    if($size == 0) {
                        $errors[] = 'Tag error';
                        break;
                    }
                }
            }
        }

        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
            $validator = new Zend_Validate_File_Extension(array('jpg', 'jpeg', 'png'), true);
            if ($validator->isValid($_FILES['image']['name'])) {
                $errors[] = 'Not image';
            }
        }


        if (empty($errors)) {
            return true;
        }

        return $errors;
    }

    public function getCategories()
    {
        $cats = Mage::getModel('magecom_blog/category')
            ->getCollection()
            ->addPostFilter($this->getId());

        $categories = array();

        foreach ($cats as $cat) {
            $categories[] = $cat->getName();
        }

        return $categories;
    }

    public function getPostTags()
    {
        $tags = Mage::getModel('magecom_blog/tag')
            ->getCollection()
            ->addPostFilter($this->getId());

        $postTags = array();

        foreach ($tags as $tag) {
            $postTags[] = $tag->getName();
        }

        return $postTags;
    }

    public function _afterSave()
    {
        $cats = $this->getCats();
        $tags = $this->getTags();

        if(is_array($cats)) {
            foreach ($cats as $cat) {
                Mage::getModel('magecom_blog/categoryPost')
                    ->setCategoryId($cat)
                    ->setPostId($this->getId())
                    ->save();
            }
        }

        if(is_array($tags)) {
            foreach($tags as $tag) {
                Mage::getModel('magecom_blog/tagPost')
                    ->setTagId($tag)
                    ->setPostId($this->getId())
                    ->save();
            }
        }

        if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '' && !$this->isSaved) {
            $fileName       = $_FILES['image']['name'];
            $fileExt        = strtolower(substr(strrchr($fileName, ".") ,1));
            $fileNamewoe    = rtrim($fileName, $fileExt);
            $fileName       = preg_replace('/\s+', '', $fileNamewoe) . time() . '.' . $fileExt;

            $uploader       = new Varien_File_Uploader('image');

            $path = Mage::getBaseDir('media') . DS . 'blog';
            if(!is_dir($path)){
                mkdir($path, 0777, true);
            }

            $uploader->save($path . DS . $this->getId(), $fileName);

            $url = 'blog/' . $this->getId() . '/' . $fileName;

            $this->isSaved = true;

            $this->setImageUrl($url)->save();
        }
    }

}