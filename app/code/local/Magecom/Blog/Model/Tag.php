<?php

class Magecom_Blog_Model_Tag extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('magecom_blog/tag');
        parent::_construct();
    }
}