<?php


class Magecom_Blog_Model_System_Config_Source_PostsMode
{
    public function toOptionArray()
    {
        return array(
            array(
                'value' => '0',
                'label' => 'Approve all',
            ),
            array(
                'value' => '1',
                'label' => 'Admin approve',
            ),
        );
    }
}
