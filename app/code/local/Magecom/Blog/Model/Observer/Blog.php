<?php

class Magecom_Blog_Model_Observer_Blog
{
    private $isCalled = false;

    public function redirect($observer)
    {
        $currentUrl = Mage::helper('core/url')->getCurrentUrl();
        $url = Mage::getSingleton('core/url')->parseUrl($currentUrl);
        $path = $url->getPath();

        $is_enabled = Mage::getStoreConfig('tab/general/enabled');

        if(!$this->isCalled && !$is_enabled && preg_match('#^/blog#', $path)) {
            Mage::app()->getFrontController()->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
            Mage::app()->getFrontController()->getResponse()->setHeader('Status', '404 File not found');

            $request = Mage::app()->getRequest();
            $request->initForward()
                ->setControllerName('indexController')
                ->setModuleName('Mage_Cms')
                ->setActionName('defaultNoRoute')
                ->setDispatched(false);

            $this->isCalled = true;
        }

        return $this;
    }
}