<?php

class Magecom_Blog_Model_Observer_Customer
{
    public function redirectCustomer($observer)
    {
        $url = Mage::getUrl('blog/');
        Mage::app()->getResponse()->setRedirect($url);

        return $this;
    }
}