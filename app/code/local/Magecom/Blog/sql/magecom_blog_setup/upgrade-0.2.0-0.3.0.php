<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn('magecom_blog_post_entity', 'is_approved', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_SMALLINT,
        'comment'   => 'Is approved',
        'nullable'  => false,
        'default'   => '2',
        'primary' => false
    ));

$installer->endSetup();