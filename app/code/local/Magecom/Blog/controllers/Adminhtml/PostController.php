<?php

class Magecom_Blog_Adminhtml_PostController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('catalog')->renderLayout();
    }

    public function newAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $testModel = Mage::getModel('magecom_blog/post')
            ->setStoreId($this->getRequest()->getParam('store', 0))
            ->load($id);

        if ($testModel->getId() || $id == 0) {
            Mage::register('post_data', $testModel);
            $this->loadLayout();
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError('Post does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost())
        {
            try {
                $postData = $this->getRequest()->getPost();
                $testModel = Mage::getModel('magecom_blog/post');

                if( $this->getRequest()->getParam('id') <= 0 ) {
                    $testModel->setCreatedAt(
                        Mage::getSingleton('core/date')
                            ->gmtDate()
                    );
                }

                $testModel->addData($postData)
                    ->setUpdatedAt(Mage::getSingleton('core/date')->gmtDate())
                    ->setStoreId($this->getRequest()->getParam('store', 0))
                    ->setId($this->getRequest()->getParam('id'))
                    ->save();

                Mage::getSingleton('adminhtml/session')->addSuccess('successfully saved');
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->settestData($this->getRequest()->getPost());

                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));

                return;
            }
        }

        $this->_redirect('*/*/');
    }

    public function deleteAction()
    {
        if($this->getRequest()->getParam('id') > 0)
        {
            try
            {
                $testModel = Mage::getModel('magecom_blog/post');
                $testModel->setId($this->getRequest()->getParam('id'))
                    ->delete();

                Mage::getSingleton('adminhtml/session')->addSuccess('successfully deleted');

                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());

                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $postIds = $this->getRequest()->getParam('posts');
        if (!is_array($postIds)) {
            $this->_getSession()->addError($this->__('Please select product(s).'));
        } else {
            if (!empty($postIds)) {
                try {
                    foreach ($postIds as $postId) {
                        $product = Mage::getSingleton('magecom_blog/post')->load($postId);
                        $product->delete();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been deleted.', count($postIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }

    public function massUpdateStatusAction()
    {
        $postIds = $this->getRequest()->getParam('posts');
        $status = $this->getRequest()->getParam('status');

        if (!is_array($postIds)) {
            $this->_getSession()->addError($this->__('Please select product(s).'));
        } else {
            if (!empty($postIds)) {
                try {
                    foreach ($postIds as $postId) {
                        $product = Mage::getSingleton('magecom_blog/post')
                            ->load($postId)
                            ->setIsApproved($status)
                            ->save();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Total of %d record(s) have been changed.', count($postIds))
                    );
                } catch (Exception $e) {
                    $this->_getSession()->addError($e->getMessage());
                }
            }
        }
        $this->_redirect('*/*/index');
    }
}