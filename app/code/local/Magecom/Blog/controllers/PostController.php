<?php

class Magecom_Blog_PostController extends Mage_Core_Controller_Front_Action
{
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getSingleton('customer/session')->authenticate($this) &&
            Mage::getStoreConfig('tab/general/only_reg') === '0'
        ) {
            $this->getResponse()->setRedirect(Mage::helper('customer')->getLoginUrl());
        }
    }

    public function createAction()
    {
        $data = $this->getRequest()->getPost();

        if ($data) {
            $post = Mage::getModel('magecom_blog/post')
                ->setData($data)
                ->setStoreId(Mage::app()->getStore()->getId());

            if (Mage::getStoreConfig('tab/general/posts_mode') === '0') {
                $post->setIsApproved(1);
            } else {

            }

            $validate = $post->validate();

            if ($validate === true) {
                $post->save();
            } else {
                $session = Mage::getSingleton('customer/session');
                $session->addError($validate);
            }
        }

        $this->_redirect('blog/');
    }
}