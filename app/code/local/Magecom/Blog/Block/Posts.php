<?php

class Magecom_Blog_Block_Posts extends Mage_Core_Block_Template
{
    public function getLastPosts()
    {
        $posts_qty = Mage::getStoreConfig('tab/general/posts_qty');

        $collection = Mage::getModel('magecom_blog/post')
            ->getCollection()
            ->addAttributeToSelect('*');

        $collection->getSelect()->limit($posts_qty);

        return $collection;
    }
}