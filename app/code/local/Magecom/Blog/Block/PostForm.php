<?php

class Magecom_Blog_Block_PostForm extends Mage_Core_Block_Template
{
    public function getCategories()
    {
        $collection = Mage::getModel('magecom_blog/category')
            ->getCollection();

        return $collection;
    }

    public function getTags()
    {
        $collection = Mage::getModel('magecom_blog/tag')
            ->getCollection();

        return $collection;
    }
}