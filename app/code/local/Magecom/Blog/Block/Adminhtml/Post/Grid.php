<?php

class  Magecom_Blog_Block_Adminhtml_Post_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Class constructor
     */
    public function __construct()
    {
        parent::__construct(); # for grids, parent constructor should be called first
        $this->setId('postGrid'); # not sure where the grid id gets used
        $this->setDefaultSort('title'); # sets the default sort column
        $this->setDefaultDir('asc'); # sets the default sort direction
        $this->setSaveParametersInSession(true); # this sets filters and sorts in the session, as opposed to using the url
    }
    /**
     * Prepare grid collection object
     *
     * @return Examples_AdminGridAndForm_Block_Adminhtml_Thing_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('magecom_blog/post')->getCollection()->addAttributeToSelect('*');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    /**
     * Prepare grid columns
     *
     * @return Examples_AdminGridAndForm_Block_Adminhtml_Thing_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'entity_id', # the column id
            array(
                'type'     => 'number', # needed for using a ranged filter
                'header'   => 'ID',
                'width'    => '10px',
                'index'    => 'entity_id', # index is the name of the data in the entity
                'sortable' => true, # defaults to true so this is pointless, just using as an example, can be true or false
            )
        );

        $this->addColumn('image_url', array(
            'header' => Mage::helper('catalog')->__('Image'),
            'align' => 'center',
            //'index' => 'image_url',
            'width'     => '97',
            'renderer' => 'Magecom_Blog_Block_Adminhtml_Template_Grid_Renderer_Image'
        ));

        $this->addColumn(
            'title',
            array(
                'header' => 'Title',
                'width'  => '600px',
                'index'  => 'title',
            )
        );

        $store = $this->_getStore();
        if ($store->getId()) {
            $this->addColumn('custom_title',
                array(
                    'header'=> Mage::helper('catalog')->__('Title in %s', $store->getName()),
                    'index' => 'custom_title',
                ));
        }

        $this->addColumn(
            'is_approved',
            array(
                'header' => 'Status',
                'width'  => '60px',
                'index'  => 'is_approved',
                'renderer' => 'Magecom_Blog_Block_Adminhtml_Template_Grid_Renderer_Status'
            )
        );

        return parent::_prepareColumns();
    }

    protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }

    /**
     * Return a URL to be used for each row
     *
     * If you don't wish rows to return a URL, simply omit this method
     *
     * @param Varien_Object $row The row for which to supply a URL
     *
     * @return string The row URL
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    /**
     * Prepare grid mass actions
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->getMassactionBlock()->setFormFieldName('posts');
        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'   => 'Delete',
                'url'     => $this->getUrl('*/*/massDelete'),
                'confirm' =>'Are you sure?'
            )
        );

        $statuses = Mage::helper('review')->getReviewStatusesOptionArray();
        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('update_status', array(
            'label'         => Mage::helper('review')->__('Update Status'),
            'url'           => $this->getUrl('*/*/massUpdateStatus'),
            'additional'    => array(
                'status'    => array(
                    'name'      => 'status',
                    'type'      => 'select',
                    'class'     => 'required-entry',
                    'label'     => Mage::helper('review')->__('Status'),
                    'values'    => $statuses
                )
            )
        ));

        return $this;
    }
}