<?php

class Magecom_Blog_Block_Adminhtml_Post_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(
            array(
                'id'      => 'edit_form',
                'action'  => $this->getUrl('*/*/save', array('id' => $this->getRequest()->getParam('id'))),
                'method'  => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        $form->setDataObject(Mage::registry('post_data'));

        $fieldset = $form->addFieldset(
            'base_fieldset',
            array(
                'legend' => Mage::helper('magecom_blog')->__('General Information'),
            )
        );

        if(!empty(Mage::registry('post_data'))) {
            $titleEA = Mage::registry('post_data')
                ->getResource()
                ->getAttribute('title');

            $descriptionEA = Mage::registry('post_data')
                ->getResource()
                ->getAttribute('description');
        }

        $fieldset->addField(
            'title',
            'text',
            array(
                'label'    => Mage::helper('magecom_blog')->__('Title'),
                'class'    => 'required-entry',
                'required' => true,
                'name'     => 'title',
                'entity_attribute' => $titleEA
            )
        )->setRenderer(
            $this->getLayout()
                ->createBlock('adminhtml/catalog_form_renderer_fieldset_element')
                //->setTemplate('catalog/form/renderer/fieldset/blog/element.phtml')
        );

        $fieldset->addField(
            'description',
            'textarea',
            array(
                'label' => Mage::helper('magecom_blog')->__('Description'),
                'class'    => 'required-entry',
                'required' => true,
                'name'  => 'description',
                'entity_attribute' => $descriptionEA
            )
        )->setRenderer(
            $this->getLayout()
                ->createBlock('adminhtml/catalog_form_renderer_fieldset_element')
                //->setTemplate('catalog/form/renderer/fieldset/blog/element.phtml')
        );

        $fieldset->addField(
            'is_approved',
            'select',
            array(
                'label'     => Mage::helper('review')->__('Status'),
                'required'  => true,
                'name'      => 'is_approved',
                'values'    => Mage::helper('review')->getReviewStatusesOptionArray(),
            )
        );

        if ( Mage::registry('post_data') )
        {
            $form->setValues(Mage::registry('post_data')->getData());
        }

        return parent::_prepareForm();
    }
}