<?php

class Magecom_Blog_Block_Adminhtml_Post_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_post';
        $this->_blockGroup = 'magecom_blog';
        $this->_updateButton('save', 'label', 'Save Post');
        $this->_updateButton('delete', 'label', 'Delete Post');
        $this->_addButton(
            'save_and_edit_button',
            array(
                'label'     => 'Save and Continue Edit',
                'onclick'   => 'saveAndContinueEdit()',
                'class'     => 'save'
            ),
            100
        );

        //$this->setTemplate('catalog/blog/post.phtml');
    }

    public function isSingleStoreMode()
    {
        if (!Mage::app()->isSingleStoreMode()) {
            return false;
        }
        return true;
    }
}