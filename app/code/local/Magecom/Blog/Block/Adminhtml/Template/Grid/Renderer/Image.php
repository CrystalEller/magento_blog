<?php

class Magecom_Blog_Block_Adminhtml_Template_Grid_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $url = Mage::getBaseUrl('media') . $row->getImageUrl();
        $out = "<img src=". $url ." width='60px'/>";
        return $out;
    }
}