<?php

class Magecom_Blog_Block_Adminhtml_Template_Grid_Renderer_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $statuses = Mage::helper('review')->getReviewStatuses();
        return $statuses[$row->getIsApproved()];
    }
}