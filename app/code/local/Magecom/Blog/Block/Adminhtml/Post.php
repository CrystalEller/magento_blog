<?php

class Magecom_Blog_Block_Adminhtml_Post extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_post';
        $this->_blockGroup = 'magecom_blog';
        $this->_headerText = 'Blog post';
        $this->_addButtonLabel = 'Add new post';
        parent::__construct();
    }
}