<?php

class Magecom_Subscribe_SubscribeController extends Mage_Core_Controller_Front_Action
{
    public function subscribeAction()
    {
        $email = $this->getRequest()->getPost('email');

        if (!Zend_Validate::is($email, 'EmailAddress')) {
            $jsonData = json_encode(array('message' => 'Invalid Email'));
        } else {
            $subscribeModel = new Mage_Newsletter_Model_Subscriber();

            if($subscribeModel->subscribe($email)){
                $jsonData = json_encode(array('message' => 'Successfully subscribe'));
            };
        }

        $this->getResponse()->setHeader('Content-type', 'application/json');
        $this->getResponse()->setBody($jsonData);
    }
}