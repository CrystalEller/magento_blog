<?php

class Magecom_CustomPriceRule_Lib_Varien_Data_Form_Element_Customprice extends Varien_Data_Form_Element_Select
{
    private $label = '';
    private $note = '';

    public function __construct($attributes=array())
    {
        $this->label = $attributes['label'];
        $this->note = $attributes['note'];

        $attributes['label'] = '';
        $attributes['note'] = '';

        parent::__construct($attributes);
    }

    public function getElementHtml()
    {
        $html = '<table class="form-list" cellspacing="0"><tr><td class="label">'.$this->label.'</td><td class="value">';
        $this->addClass('select');
        $html.= '<select id="'.$this->getHtmlId().'" name="'.$this->getName().'" '.$this->serialize($this->getHtmlAttributes()).'>'."\n";

        $value = $this->getValue();
        if (!is_array($value)) {
            $value = array($value);
        }

        if ($values = $this->getValues()) {
            foreach ($values as $key => $option) {
                if (!is_array($option)) {
                    $html.= $this->_optionToHtml(array(
                        'value' => $key,
                        'label' => $option),
                        $value
                    );
                }
                elseif (is_array($option['value'])) {
                    $html.='<optgroup label="'.$option['label'].'">'."\n";
                    foreach ($option['value'] as $groupItem) {
                        $html.= $this->_optionToHtml($groupItem, $value);
                    }
                    $html.='</optgroup>'."\n";
                }
                else {
                    $html.= $this->_optionToHtml($option, $value);
                }
            }
        }

        $html.= '</select>'."\n";
        $html.= $this->getAfterElementHtml();
        $html.= '<p class="note" id="note_uses_per_customer"><span>'.$this->note.'</span></p></td></tr></table>';
        return $html;
    }
}