<?php

class Magecom_CustomPriceRule_Model_Observer
{
    public function turnOffDiscount(Varien_Event_Observer $observer)
    {
        $result = $observer->getData('result');
        $rule = $observer->getData('rule');
        $product = $observer->getData('item')->getProduct();

        $catalogPriceRule = Mage::getModel('catalogrule/rule')->calcProductPriceRule($product, $product->getPrice());

        $toDate = $product->getSpecialToDate();
        $fromDate = $product->getSpecialFromDate();
        $today = strtotime(Mage::getModel('core/date')->date('Y-m-d H:i:s'));

        if($rule->getCustomPriceRule() == 1) {
            if ($product->getSpecialPrice()) {
                if(!($today >= strtotime($fromDate) &&
                    $today <= strtotime($toDate) ||
                    $today >= strtotime($fromDate) &&
                    is_null($toDate))
                ) {
                    $result->setDiscountAmount(0);
                    $result->setBaseDiscountAmount(0);
                }
            }

            if ($catalogPriceRule) {
                $result->setDiscountAmount(0);
                $result->setBaseDiscountAmount(0);
            }
        }

        return $this;
    }
}