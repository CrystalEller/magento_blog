<?php

$installer = $this;
$installer->startSetup();

$installer->getConnection()
    ->addColumn('salesrule', 'custom_price_rule', array(
        'type'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment'   => 'Custom Price Rule Discount',
        'unsigned' => true,
        'primary' => false
    ));

$installer->endSetup();