jQuery(document).ready(function(){
    $j('#subscribe').submit(function(e){

        var form = new VarienForm('subscribe', true);

        if (form.validator && form.validator.validate())
        {
            $j.ajax({
                url: $j(this).attr('action'),
                type: 'POST',
                dataType: 'json',
                data: {
                    email: $j(this).find('input[name="email"]').val()
                },
                success: function(data) {
                    $j('#subscribe').find('.message').text(data['message']).fadeOut(2000);
                },
                error: function() {
                    alert('error');
                }
            });
        }

        e.preventDefault();
    });
});